; Load custom configuration
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
  (package-initialize)
  (package-refresh-contents)

  )

;; (if (file-exists-p "~/.emacs.d/custom_confs/42_header_maker/init.el") (load-file "~/.emacs.d/custom_confs/42_header_maker/init.el"))
(if (file-exists-p "~/.emacs.d/custom_confs/basic_conf.el") (load-file "~/.emacs.d/custom_confs/basic_conf.el"))
(if (file-exists-p "~/.emacs.d/custom_confs/ide_like.el") (load-file "~/.emacs.d/custom_confs/ide_like.el"))
(if (file-exists-p "~/.emacs.d/custom_confs/shortcuts.el") (load-file "~/.emacs.d/custom_confs/shortcuts.el"))
(if (file-exists-p "~/.emacs.d/custom_confs/prog_languages.el") (load-file "~/.emacs.d/custom_confs/prog_languages.el"))
(if (file-exists-p "~/.emacs.d/custom_confs/style.el") (load-file "~/.emacs.d/custom_confs/style.el"))
(if (file-exists-p "~/.emacs.d/custom_confs/music.el") (load-file "~/.emacs.d/custom_confs/music.el"))

(add-to-list 'auto-mode-alist '("\\.krakrc\\'" . shell-script-mode))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
	(emms markdown-mode terraform-mode dockerfile-mode web-mode yaml-mode go-mode color-identifiers-mode beacon rainbow-delimiters smex dumb-jump auto-complete)))
 '(terraform-indent-level 4))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-group-tag ((t (:inherit variable-pitch :foreground "color-26" :weight bold :height 1.2))))
 '(custom-variable-tag ((t (:foreground "color-27" :weight bold))))
 '(font-lock-function-name-face ((t (:foreground "color-136"))))
 '(font-lock-keyword-face ((t (:foreground "color-241"))))
 '(font-lock-string-face ((t (:foreground "color-22"))))
 '(highlight-indentation-face ((t nil)))
 '(hl-line ((t (:background "color-233"))))
 '(lazy-highlight ((t (:background "color-52"))))
 '(magit-diff-context-highlight ((t (:background "brightblack" :foreground "white"))))
 '(magit-diff-hunk-heading-highlight ((t (:background "color-19" :foreground "grey30"))))
 '(popup-face ((t (:inherit default :background "black" :foreground "color-243"))))
 '(popup-scroll-bar-background-face ((t (:background "black"))))
 '(popup-summary-face ((t (:inherit popup-face :background "black" :foreground "dimgray"))))
 '(popup-tip-face ((t (:background "brightblack" :foreground "black"))))
 '(region ((t (:background "color-234"))))
 '(show-paren-match ((t (:background "brightblack" :foreground "grey"))))
 '(whitespace-indentation ((t (:foreground "black"))))
 '(whitespace-line ((t (:foreground "black"))))
 '(whitespace-space ((t (:background "color-237" :foreground "lightgray"))))
 '(whitespace-tab ((t (:foreground "black")))))

;;+ ++++++++++++++++++++++++++++++++++++++++++++++++++
;; === IDE Like === ;;
;; --- AutoComplete
(unless (package-installed-p 'auto-complete)
  (package-install 'auto-complete))
(global-auto-complete-mode t)

;; --- Highlighting remains after lsearch
(setq lazy-highlight-cleanup nil)

;; --- Org-mode
(unless (package-installed-p 'org)
  (package-install 'org))

;; --- enable clipboard in emacs
(setq x-select-enable-clipboard t)

;; --- Auto-completion
(ac-config-default)

;; --- show starting+ending brackets
(show-paren-mode 1)
(set-face-background 'show-paren-match "brightblack")
(set-face-foreground 'show-paren-match "white")

;; --- Remove spaces at the end of lines before saving file
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(put 'scroll-left 'disabled nil)

;; --- Jump to definition
(global-set-key (kbd "C-M-z") 'dumb-jump-go)

;; --- Smex: buffer history
(unless (package-installed-p 'smex)
  (package-install 'smex))
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)

;; --- Rainbow-delimiters
;; Highlights delimiters such as parentheses, brackets or braces according to their depth
(unless (package-installed-p 'rainbow-delimiters)
  (package-install 'rainbow-delimiters))
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;; --- Beacon Cursor
;; Whenever the window scrolls a light will shine on top of your cursor.
(unless (package-installed-p 'beacon)
  (package-install 'beacon))
(beacon-mode 1)

;; --- Color identifier
;; Highlights each source code identifier uniquely based on its name.
(unless (package-installed-p 'color-identifiers-mode)
  (package-install 'color-identifiers-mode))


;; --- Show current filepath
(defun show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message (buffer-file-name)))
(global-set-key [C-f1] 'show-file-name)


;; --- Collapse/Expand block
(add-hook 'prog-mode-hook #'hs-minor-mode)
(global-set-key (kbd "C-c <right>") 'hs-show-block)
(global-set-key (kbd "C-c <left>") 'hs-hide-block)
(global-set-key (kbd "C-c <prior>") 'hs-hide-level)

;; --- NeoTree
;; Tree tab to navigate in directories
;; Projectile + helm are used to updated the project shown by neotree
;; --- Shortcuts
;; F8 -> open neotree and refresh project
;; F9 -> close neotree
(unless (package-installed-p 'projectile)
  (package-install 'projectile))
(require 'projectile)
(unless (package-installed-p 'helm)
  (package-install 'helm))
(require 'helm)
(unless (package-installed-p 'project-explorer)
  (package-install 'project-explorer))

(global-set-key [f8] 'project-explorer-open)

;; --- Highlight symbols
;; Highlight currently hovered expression accross a file
;; --- Command:
;; highlight-symbol -> Highlight current expression
;; highlight-symbol-next -> jump to next occurence
;; highlight-symbol-prev -> jumb to previous occurence
;; highlight-symbol-remove-all -> UN-highlight everything
(unless (package-installed-p 'highlight-symbol)
  (package-install 'highlight-symbol))
(require 'highlight-symbol)

;; GoTo-line-preview
;; Preview line when executing goto-line command.
(unless (package-installed-p 'goto-line-preview)
  (package-install 'goto-line-preview))
(global-set-key [remap goto-line] 'goto-line-preview)

;; Highligh current lines
(global-hl-line-mode 1)

;; Google-this
;; Search in google using mini-buffer
(unless (package-installed-p 'google-this)
  (package-install 'google-this))
(google-this-mode 1)
(global-set-key (kbd "C-c C-g") 'google-this-ray)

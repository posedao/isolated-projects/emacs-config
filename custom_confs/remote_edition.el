;; === TRAMP ===
; --- Set the default network protocol to use
(setq tramp-default-method "ssh")

;; --- Edition in Container
;; HowTo
;; Ctrl+x -> Ctrl+f
;; /docker:DOCKER_ID:/path/to/file
(unless (package-installed-p 'docker-tramp)
  (package-install 'docker-tramp))

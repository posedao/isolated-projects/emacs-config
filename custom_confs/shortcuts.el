;;+ ++++++++++++++++++++++++++++++++++++++++++++++++++
;; === SHORTCUTS === ;;

;; --- replace killing all buffer, by just the current one
(global-set-key (kbd "C-x k") 'kill-this-buffer)


;; --- WINDOWS ACTIONS ---
;; --- Delete current window
(global-set-key (kbd "C-x w") 'delete-window)

;; --- Spliter
(defun empty-current-window ()
  (interactive)
  (let (($buf (generate-new-buffer "untitled")))
    (switch-to-buffer $buf)
    (funcall initial-major-mode)
    (setq buffer-offer-save t)
    $buf
    ))

(defun split-vertically ()
  (interactive)
  (split-window-right)
  (windmove-right)
  (empty-current-window)
  )

(defun split-horizontally ()
  (interactive)
  (split-window-below)
  (windmove-down)
  (empty-current-window)
  )

(global-set-key (kbd "C-M-@") 'split-vertically)
(global-set-key (kbd "M-SPC") 'split-horizontally)
;(windmove-default-keybindings)  ; Uses [Shift + arrows]


;; --- TEXT INSERTION
(defun ipdb ()
  "Insert necessary pdb package to debug in python"
  (interactive)
  (insert "import pdb; pdb.set_trace()")
  (indent-for-tab-command)
  )

(defun irdb ()
  "Insert necessary pdb package to debug in a celery task"
  (interactive)
  (insert "from celery.contrib import rdb; rdb.set_trace()")
  )

(defun ipprint ()
  "Insert necessary pprint package to use it"
  (interactive)
  (insert "from pprint import pprint as pp")
  )

;; -- Header
(defun py_headers ()
  "Insert python headers"
  (interactive)
  (insert "#!/usr/bin/python\n# coding: utf-8\n")
  )

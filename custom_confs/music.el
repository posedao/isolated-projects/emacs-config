;;+ ++++++++++++++++++++++++++++++++++++++++++++++++++
;; === EMMS === ;;

(unless (package-installed-p 'emms)
  (package-install 'emms))

(setq exec-path (append exec-path '("/usr/bin")))
(add-to-list 'load-path "~/.emacs.d/site-lisp/emms/lisp")
(require 'emms-setup)
(require 'emms-player-mplayer)
(emms-standard)
(emms-default-players)
(define-emms-simple-player mplayer '(file url)
      (regexp-opt '(".ogg" ".mp3" ".wav" ".mpg" ".mpeg" ".wmv" ".wma"
                    ".mov" ".avi" ".divx" ".ogm" ".asf" ".mkv" "http://" "mms://"
                    ".rm" ".rmvb" ".mp4" ".flac" ".vob" ".m4a" ".flv" ".ogv" ".pls"))
      "mplayer" "-slave" "-quiet" "-really-quiet" "-fullscreen")


;; --- Usefull commands
;; emms-add-directory-tree
;; emms-repeat-curr-track
(global-set-key (kbd "M-+") 'emms-seek-forward)
(global-set-key (kbd "M-_") 'emms-seek-backward)
